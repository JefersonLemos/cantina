<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('produtos', 'ProductController');
Route::resource('clientes', 'ClienteController');
Route::get('vendas/carrinho/{idcliente}', 'VendaController@preenchercarrinho')->name('vendas.carrinho');
Route::get('vendas/cancelar/{local1}', 'VendaController@cancelaCompra')->name('vendas.cancelar');
Route::get('/vendas/CancelaCompraUnitaria/{id_cliente}/{id_produto}', 'VendaController@CancelaCompraUnitaria')->name('vendas.CancelaCompraUnitaria');
Route::get('vendas/encerrar-compra/{id_cliente}', 'VendaController@encerrarCompra');
Route::post('vendas/comprar/{id_cliente}', 'VendaController@comprar')->name('vendas.comprar');
Route::resource('vendas', 'VendaController');
Route::post('clientes/transacao/{id}', 'ClienteController@transacao')->name('clientes.transacao');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
