#Projeto Cantina

Esse projeto tem por objetivo avaliar o processo de desenvolvimento de um software na disciplina de engenharia de software do curso de Ciencias da Computacao do Instituto Federal Cararinense - Campus Videira.

Para executar esse projeto execute:

```php -S localhost:8000```

## Depêndencias

* PHP5 >= 5.5
* Mysql Server >= 5.5.54
* Apache2
