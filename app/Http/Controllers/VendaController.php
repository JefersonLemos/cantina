<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venda;
use App\Cliente;
use App\Product;
use App\VendaProduto;

class VendaController extends Controller
{
    
     //esperimentar $merge = array_marge($a, $b);
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function cancelaCompra(Request $request, $local){
        $request->session()->forget('produtosCompra');//limpa a session produtosCompra
        $request->session()->forget('totalCompra');//limpa a session totalCompra
        if($local == 1){// se $local for = 1 volta para tela /vendas --> index
             $clientes = Cliente::all();
            return view('vendas/index')->with('clientes', $clientes);
        }else{ //se outra coisa volta para raiz
            return view('/home');
        }
    }

    public function CancelaCompraUnitaria(Request $request, $idCliente, $idProduto){
        $listaProdutos = unserialize($request->session()->get('produtosCompra'));
        //dd($listaProdutos);
        $produto = Product::find($idProduto);
        $totalCompra = $request->session()->get('totalCompra') - ($listaProdutos[$idProduto] * $produto->valor);
        $request->session()->put('totalCompra',$totalCompra);

        unset($listaProdutos[$idProduto]);
        
        $produtos_s = serialize($listaProdutos);    
        $request->session()->put('produtosCompra', $produtos_s);
        return redirect()->route('vendas.show', $idCliente);
    }

    public function encerrarCompra(Request $request, $idCliente){
        
        $listaProdutos = unserialize($request->session()->get('produtosCompra'));
        $vendas = new Venda();
        $vendas -> idcliente = $idCliente;
        $vendas -> valortotalcompra = $request->session()->get('totalCompra');
        $vendas->save();

        $vendas = Venda::all();
        $idVendas = count($vendas);
        foreach($listaProdutos as $idProduto=>$qtd){
            $produtoscomprados = new VendaProduto();
            $produtos = Product::find($idProduto);
            $produtoscomprados -> idproduto = $idProduto;
            $produtoscomprados -> valor = $produtos->valor;
            $produtoscomprados -> idvendas = $idVendas;
            $produtoscomprados -> quantia = $qtd;
            $produtoscomprados -> save();
        }

        $cliente = Cliente::find($idCliente);
        $cliente->saldo = $cliente->saldo - $request->session()->get('totalCompra');
        $cliente -> save();

        $request->session()->forget('produtosCompra');
        $clientes = Cliente::all();
        $request->session()->forget('totalCompra');
        return view('vendas/index')->with('clientes', $clientes);
    }

    public function comprar(Request $request, $idCliente){
        $cliente = Cliente::find($idCliente);
        $idProduto = $request->input("idproduto");
        $produto = Product::find($idProduto);
    
        $qtd = $request->input('quantidade');

        if($qtd >= 1){
            $valorcompra = $request->input('quantidade') * $produto->valor;
            $totalCompra = $request->session()->get('totalCompra') + $valorcompra;
            $request->session()->put('totalCompra',$totalCompra);

            $listaProdutos = unserialize($request->session()->get('produtosCompra'));

            if ($listaProdutos == null){
                $listaProdutos = [];
            }
            $testeExistencia = 0;
            if(count($listaProdutos)!= 0){
                foreach($listaProdutos as $idProdutoExistente=>$qtd){
                    if($idProduto == $idProdutoExistente){
                        $testeExistencia = 1;
                        break;
                    }
                }
            }
            if($testeExistencia == 1){
                $listaProdutos[$idProduto] = $listaProdutos[$idProduto] + $request->input('quantidade');
                $testeExistencia = 0;
            }else{
                $listaProdutos[$idProduto] = $request->input('quantidade');
            }
            $produtos_s = serialize($listaProdutos);
            
            $request->session()->put('produtosCompra', $produtos_s);
        }else{
            $request -> session() -> flash('message', 'Quantidade menor que 1 não permitida para compras!!!');
        }
       
        return redirect()->route('vendas.show', $cliente);
    }

    public function index(){
        $vendas = Venda::all();
        $clientes = Cliente::all();
        return view('vendas/index')->with('clientes', $clientes);
    }
    public function show(Request $request, $id){
      // return "quero ve funciona -- {$id}";
        $listaProdutos = unserialize($request->session()->get('produtosCompra'));
        $produtos = Product::all();
        $cliente = Cliente::find($id);
        $produtosCompradosNome = [];
        $produtosCompradosValor = [];
        $produtosCompradosQtd = [];
        $produtosCompradosID = [];
        $tamanhoArrayPCN = 0; //Tamanho do array produtosCompradosNome
        if($listaProdutos != null){
            foreach($listaProdutos as $idProduto => $qtd){
                $produto = Product::find($idProduto);
                $produtosCompradosNome[] = $produto->nome;
                $produtosCompradosValor[] = $produto->valor;
                $produtosCompradosQtd[] = $qtd;
                $produtosCompradosID[] = $produto->id;
            }
        }
        $tamanhoArrayPCN = count($produtosCompradosNome);
        return view('vendas/show')->with([
            'cliente'=>$cliente, 
            'produtos'=>$produtos, 
            'totalCompra'=> $request->session()->get('totalCompra'),
            'produtosCompradosNome'=>$produtosCompradosNome,
            'produtosCompradosValor'=>$produtosCompradosValor,
            'produtosCompradosQtd'=>$produtosCompradosQtd,
            'tamanhoArrayPCN'=>$tamanhoArrayPCN,
            'produtosCompradosID'=>$produtosCompradosID
            ]);
    }
  
    public function create(){
        $vendas = new Vendas();

        return view('vendas/create')->with('vendas', $vendas);

    }

    /*
    public function edit($id){
        $produto = Product::find($id);

        return view('products/edit')->with('produto', $produto);
    }

    public function destroy(Request $request, $id){
        $produto = Product::find($id);
        if ($produto -> delete()) {
            $request -> session() -> flash('message', 'Produto Excluído');
        } else {
            $request -> session() -> flash('message', 'Houve falha ao excluir');
        }

        return redirect()->route('produtos.index');        
    }

    public function store(request $request){

        $produto = new product();
        $produto -> nome = $request->input('nome');
        $produto -> valor = $request->input('valor');

        if($produto->save()){
            $request->session()->flash('message', 'Produto castrado com sucesso');
            //echo 'Produto salvo com sucesso';
        }else{
            $request->session()->flash('message', 'Houve um erro ao cadastrar o produto');
            //echo 'Houve um erro ao salvar';
        }
        return redirect()->route('produtos.index');
    }


    public function update(request $request, $id){

        $produto = Product::find($id);
        $produto -> nome = $request->input('nome');
        $produto -> valor = $request->input('valor');

        if($produto->save()){
            $request->session()->flash('message', 'Produto atualizado com sucesso');
            //echo 'Produto salvo com sucesso';
        }else{
            $request->session()->flash('message', 'Houve um erro ao salvar');
            //echo 'Houve um erro ao salvar';
        }
        return redirect('produtos');
    }
    */
}
