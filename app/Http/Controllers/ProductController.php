<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function index(){
        $produtos = Product::all();

        return view('products/index')->with('produtos', $produtos);
    }
    public function show($id){
        $produto = Product::find($id);

        return view('products/show')->with('produto', $produto);
    }

    public function create(){
        $produto = new Product();

        return view('products/create')->with('produto', $produto);

    }
    public function edit($id){
        $produto = Product::find($id);

        return view('products/edit')->with('produto', $produto);
    }
 
    public function destroy(Request $request, $id){
        $produto = Product::find($id);
        if ($produto -> delete()) {
            $request -> session() -> flash('message', 'Produto Excluído');
        } else {
            $request -> session() -> flash('message', 'Houve falha ao excluir');
        }

        return redirect()->route('produtos.index');        
    }

    public function store(request $request){

        $produto = new product();
        $produto -> nome = $request->input('nome');
        $produto -> valor = $request->input('valor');

        if($produto->save()){
            $request->session()->flash('message', 'Produto castrado com sucesso');
            //echo 'Produto salvo com sucesso';
        }else{
            $request->session()->flash('message', 'Houve um erro ao cadastrar o produto');
            //echo 'Houve um erro ao salvar';
        }
        return redirect()->route('produtos.index');
    }


    public function update(request $request, $id){

        $produto = Product::find($id);
        $produto -> nome = $request->input('nome');
        $produto -> valor = $request->input('valor');

        if($produto->save()){
            $request->session()->flash('message', 'Produto atualizado com sucesso');
            //echo 'Produto salvo com sucesso';
        }else{
            $request->session()->flash('message', 'Houve um erro ao salvar');
            //echo 'Houve um erro ao salvar';
        }
        return redirect('produtos');
    }

}
