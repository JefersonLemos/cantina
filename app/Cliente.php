<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public function conta() {
        return $this->hasOne('App\Conta');
    }
}
