<html>
	<head>
		<meta charset="utf-8" />
	</head>
<body>

<?php

	echo 'Hello World. <br/>';

	$nome = "Jeferson";

	echo 'Hello ' . $nome . '<br/>';
	echo "Hello $nome <br/>";
	
	$num1 = 10;
	$num2 = 30;

	echo $num1 + $num2 . '<br/>';
	echo $num1 - $num2 . '<br/>';
	echo $num1 * $num2 . '<br/>';
	echo $num1 / $num2 . '<br/>';

	$lista = [];
	$lista[] = 'joão';
	$lista[] = 'maria';
	$lista[] = 'Mohammed';

	for($i = 0; $i < count($lista); $i++){
		echo "O nome é: $lista[$i] <br/>";
	}

	echo "<br/>";

	foreach($lista as $item){
		echo "O nome é: $item <br/>";
	}

	$teste =true;

	/* == - igual
		 != - diferente
		 || - ou 
		 && - e
		 or - ou com baixa precedencia
		 and - e 	com baixa precedencia
		 === - igual (verificando valor e tipagem)
	*/

	if($teste === true){
		echo 'É verdadeiro <br />';
	}else{
		echo 'É falso <br />';
	}

	echo 'A data e hora atual é ' . date('d/m/Y H:i:s'); 

?>

	</body>
</html>
