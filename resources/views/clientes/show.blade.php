@extends('welcome')

@section('content')
<h1>Atualização saldo</h1>
<hr />
<?php 
  $valor = 0.0;
?>
<table class="table table-bordered">
  <tr>
  <th>Nome</th>
  <th>CPF</th>
  <th>Telefone</th>
  <th>Endereço</th>
  <th>Saldo</th>
  </tr>
    <tr>
      <td>{{ $cliente -> nome}}</td>
      <td>{{ $cliente -> cpf}}</td>
      <td>{{ $cliente -> telefone}}</td>
      <td>{{ $cliente -> endereco}}</td>
      <td>{{ $cliente -> saldo}}</td>
    </tr>
  </table>

<table class="table table-bordered">
  <tr>
  <th>Valor Da Operação</th>
  <th>Creditar</th>
  <th>Debitar</th>
  </tr>
    <tr>
    {{ Form::open(array('url' => "clientes/transacao/$cliente->id" )) }}
      <td>{{ Form::text('valor', $valor) }}</td>
      <td>
        <input class="btn btn-default" arial-label="Mostrar Cliente" type="submit" name="creditar" value="creditar">
          <span class="glyphicon glyphicon-eye-open"
          arial-hidden="true"></span>
        </input></td>
         <td><input class="btn btn-default"
          arial-label="Mostrar Cliente" type="submit" name="debitar" value="debitar">
          <span class="glyphicon glyphicon-eye-open"
          arial-hidden="true"></span>
        </input></td>

        {{ Form::close() }}
    </tr>
  </table>

<a href="/home" class="btn btn-success pull-right"
arial-label="Menu Principal">Menu Principal
</a>


<!--
<h1>Clintes Cadastrados</h1>
<div class = 'row'>
  <dl class="dl-horizontal">
    <dt>Nome:</dt>
    <dd>{{$cliente->nome }}</dd>
    <dt>CPF:</dt>
    <dd>{{ $cliente->cpf }}</dd>
    <dt>Telefone:</dt>
    <dd>{{ $cliente->telefone }}</dd>
    <dt>Endereço:</dt>
    <dd>{{ $cliente->endereco }}</dd>
    <dt>Criado em:</dt>
    <dd>{{ $cliente->created_at->format('d/m/Y h:i') }}</dd>
    <dt>Atualizado em:</dt>
    <dd>{{ $cliente->updated_at->format('d/m/Y h:i') }}</dd>
  </dl>
</div>
-->
@endsection