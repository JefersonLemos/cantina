@extends('layouts.app')
    <style>
            body{
                background-image: url('http://www.caldodepinto.com.br/2014/wp-content/uploads/2014/11/fundo-banners.jpg');
                background-repeat: no-repeat;
                background-size: cover;
                background-position: fixed;
            }
    </style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="background-color:#33CCB3; color:black">
                <div class="panel-heading" style="background-color:#33CCB3">
                    <h1>Você esta logado na Cantina Da Nona!!!</h1></div>
                <div class="panel-body panel-heading">
                    <a style="color:black" href="/vendas"><h4>Vendas</h4></a>
                </div>

                <div class="panel-body">
                   <a style="color:black" href="/produtos"><h4>Produtos</h4></a>
                </div>
                <div class="panel-body panel-heading">
                   <a style="color:black" href="/clientes"><h4>Clientes</h4></a>
                </div>
                <div class="panel-body">
                   <a style="color:black" href="/register"><h4>Cadastro de Usuários</h4></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
