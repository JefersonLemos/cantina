<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Projeto Cantina</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <style>
            body{
                background-image: url('storage/cantina.jpg');
                background-repeat: no-repeat;
                background-size: cover;
                background-position: fixed;
            }
            table{
                background-color: #33CCB3;
            }
            h6{
                font-size: 100px;
                text-align: center;
                font-weight: 900;
            }
        </style>

    </head>

        <body>
            
            <div class="container">
            <h6>Cantina Da Nona</h6>
            @if (Route::has('login'))
                    <div class="top-right links" align="center">
                        @if (Auth::check())
                            <a href="{{ url('/home') }}"><button style="background-color: #33CCB3"><h4>Menu Principal</h4></button></a>
                        @else
                            <a href="{{ url('/login') }}"><button style="background-color: #33CCB3"><h4>Login</h4</button></a>
                            <a href="{{ url('/register') }}"><button style="background-color: #33CCB3"><h4>Cadastro</h4></button></a>
                        @endif
                    </div>
            @endif
            
                @yield('content')
                
            </div>
    </body>
</html>
