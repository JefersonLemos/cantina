@extends('welcome')

@section('content')
  <div class="row">
<h1>Escolha do cliente comprador</h1>
<hr />
  <br />
  <br />
  <br />
</div>

<div class="row">
  @if(Session::has('message'))
    <div class="alert alert-success">
      <em> {!! session('message') !!}</em>
      </div>
  @endif

<table class="table table-bordered">
  <tr>
  <th>Nome</th>
  <th>CPF</th>
  <th>Saldo</th>
  <th>Selecionar</th>
  </tr>

    @foreach($clientes as $cliente)
    <tr>
      <td>{{ $cliente -> nome}}</td>
      <td>{{ $cliente -> cpf}}</td>
      <td>{{ $cliente -> saldo}}</td>
      
      <td>
        <a href="/vendas/{{$cliente->id}}" class="btn btn-default" 
          arial-label="Mostrar Cliente">
          <span class="glyphicon glyphicon-eye-open"
          arial-hidden="true"></span>
        </a>
      </td>
    </tr>

    @endforeach
  </table>
</div>
@endsection