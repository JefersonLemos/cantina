@extends('welcome')

@section('content')
<?php 
  $local1=1;
  $local2=2;
?>

<h1>Venda para {{$cliente->nome}}</h1>
<hr />


@if(Session::has('message'))
    <div class="alert alert-success">
      <em> {!! session('message') !!}</em>
    </div>
@endif
<hr />

<table class="table table-bordered">
  <tr>
    <th>Produto</th>
    <th>Quantia</th>
    <th>Compra</th>
  </tr>
  <tr>
  {{ Form::open(array('url' => "vendas/comprar/$cliente->id" )) }}
    <td>
      <select name="idproduto">
        @foreach($produtos as $produto)
          <option value="{{ $produto->id }}">{{ $produto->nome }}</option>
        @endforeach  
      </select>
    </td>
    <td>{{ Form::number('quantidade', '1') }}</td>
    <td>{{ Form::submit('Comprar', array('class' => 'btn btn-success'))}}</td>
   {{ Form::close() }}
  </tr>
</table>

<table class="table table-bordered">
  <tr>
  <th>Total Compra</th>
  <th>Finalizar Compra</th>
  </tr>
    <tr>
      <td>{{ $totalCompra }}</td>
      <td><a href="/vendas/encerrar-compra/{{ $cliente->id }}" class="btn btn-danger pull-left">
            Efetuar Compra
          </a>
      </td>
    </tr>
  </table>

<table class="table table-bordered">
  <tr>
    <th>Nome</th>
    <th>Valor</th>
    <th>Quantidade</th>
    <th>cancelar</th>
  </tr>
  @for($i = 0; $i < $tamanhoArrayPCN; $i++)
        <tr>
          <td>{{ $produtosCompradosNome[$i] }}</td>
          <td>{{ $produtosCompradosValor[$i] }}</td>
          <td>{{ $produtosCompradosQtd[$i] }}</td>
          <td><a class="btn btn-danger" href="/vendas/CancelaCompraUnitaria/{{ $cliente->id }}/{{ $produtosCompradosID[$i] }}"><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
  @endfor
</table>

<!--<table class="table table-bordered">
  <tr>
    <th>Nome</th>
    <th>Valor</th>
    <th>Quantidade</th>
    <th>Comprar</th>
  </tr>
  @foreach($produtos as $produto)
    {{ Form::open(array('url' => "vendas/comprar/$cliente->id/$produto->id" )) }}
        <tr>
          <td>{{ $produto->nome }}</td>
          <td>{{ $produto->valor }}</td>
          <td>{{ Form::text('quantidade', '3') }}</td>
          <td>{{ Form::submit('Comprar', array('class' => 'btn btn-success'))}}</td>
        </tr>
    {{ Form::close() }}
  @endforeach
</table>-->
<a href="/vendas/cancelar/{{$local2}}" class="btn btn-success pull-right">
  Menu Principal
</a>
<a href="/vendas/cancelar/{{$local1}}" class="btn btn-warning pull-right" style="margin-right: 10px">
  Voltar
</a>
@endsection