@extends('welcome')

@section('content')
<h1>Produto</h1>
<hr />
<table class="table table-bordered">
  <tr>
    <th>Nome</th>
    <th>Valor</th>
    <th>Criado em</th>
    <th>Atualizado em</th>
  </tr>
  <tr>
    <td>{{$produto->nome }}</td>
    <td>{{$produto->valor }}</td>
    <td>{{$produto->created_at->format('d/m/Y h:i') }}</td>
    <td>{{ $produto->updated_at->format('d/m/Y h:i') }}</td>
  </tr>
</table>
<a href="/home" class="btn btn-success pull-right">
  Menu Principal
</a>
<a href="/produtos" class="btn btn-info pull-right" style="margin-right: 10px">
  Voltar
</a>
@endsection