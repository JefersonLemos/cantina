@extends('welcome')

@section('content')
  <h2>Edição de Produto<h2>
  <hr />

  {{ Form::model($produto, array('route'=>array('produtos.update', $produto->id), 'method' => 'PUT')) }}
    
    {{Form::label('nome', 'Nome:')}}
    {{Form::text('nome', null, array('class' => 'form=control'))}}

    {{Form::label('valor', 'Valor:')}}
    {{Form::text('valor', null, array('class' => 'form=control'))}}

    {{Form::submit('Salvar', array('class' => 'btn btn-success'))}}

  {{Form::close()}}
@endsection