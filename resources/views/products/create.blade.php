@extends('welcome')

@section('content')
  <h2>Cadastro de Produto<h2>
  <hr />

  {{ Form::model($produto, array('route'=>array('produtos.store'))) }}
    
    {{Form::label('nome', 'Nome:')}}
    {{Form::text('nome')}}

    {{Form::label('valor', 'Valor:')}}
    {{Form::text('valor')}}

    {{Form::submit('Salvar', array('class' => 'btn btn-success'))}}

  {{Form::close()}}
@endsection