@extends('welcome')

@section('content')
  <div class="row">
<h1>Produtos</h1>
<hr />
<a href="/home" class="btn btn-success pull-right">
  Menu Principal
</a>

<a href="/produtos/create" class="btn btn-info pull-right" style="margin-right: 10px">
  Novo Produto
</a>
  <br />
  <br />
  <br />
</div>

<div class="row">
  @if(Session::has('message'))
    <div class="alert alert-success">
      <em> {!! session('message') !!}</em>
      </div>
  @endif

<table class="table table-bordered">
  <tr>
  <th>ID</th>
  <th>Nome</th>
  <th>Valor</th>
  <th>Visualizar</th>
  <th>Editar</th>
  <th>Apagar</th>
  </tr>

    @foreach($produtos as $produto)
    <tr>
      <td>{{ $produto -> id}}</td>
      <td>{{ $produto -> nome}}</td>
      <td>{{ $produto -> valor}}</td>
      <td>
        <a href="/produtos/{{$produto->id}}" class="btn btn-default"
          arial-label="Mostrar Produto">
          <span class="glyphicon glyphicon-eye-open"
          arial-hidden="true"></span>
        </a>
      </td>
      <td>
        <a href="/produtos/{{$produto->id}}/edit" class="btn btn-default"
          arial-label="Editar Produto">
          <span class="glyphicon glyphicon-pencil"
          arial-hidden="true"></span>
        </a>
      </td>
      <td>
        <!--Botão delete produtos-->
        <a style="display: inline-block">
        {{ Form::open( array('url' => "produtos/$produto->id") ) }}
          {{ Form::hidden('_method', 'DELETE') }}
          {{ Form::button('<span class="glyphicon glyphicon-trash"></span>', array('class' => 'btn btn-warning', 'type' => 'submit')) }}
        {{ Form::close()}}
        </a>
      </td>
    </tr>

    @endforeach
  </table>
</div>
@endsection