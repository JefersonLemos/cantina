@extends('welcome')

@section('content')
<h1>Atualização saldo</h1>
<hr />

<table class="table table-bordered">
  <tr>
  <th>ID</th>
  <th>Saldo</th>
  <th>ID Cliente</th>
  </tr>
    <tr>
      <td>{{ $conta -> id}}</td>
      <td>{{ $conta -> saldo}}</td>
      <td>{{ $conta -> cliente_id}}</td>
    </tr>
  </table>

<table class="table table-bordered">
  <tr>
  <th>Saldo</th>
  <th>Creditar</th>
  <th>Debitar</th>
  </tr>
    <tr>
      <td>{{ $conta -> saldo}}</td>
      <td><a href="/clientes/{{$cliente->id}}" class="btn btn-default"
          arial-label="Mostrar Cliente">
          <span class="glyphicon glyphicon-eye-open"
          arial-hidden="true"></span>
        </a></td>
         <td><a href="/clientes/{{$cliente->id}}" class="btn btn-default"
          arial-label="Mostrar Cliente">
          <span class="glyphicon glyphicon-eye-open"
          arial-hidden="true"></span>
        </a></td>
    </tr>
  </table>


<!--
<h1>Clintes Cadastrados</h1>
<div class = 'row'>
  <dl class="dl-horizontal">
    <dt>Nome:</dt>
    <dd>{{$cliente->nome }}</dd>
    <dt>CPF:</dt>
    <dd>{{ $cliente->cpf }}</dd>
    <dt>Telefone:</dt>
    <dd>{{ $cliente->telefone }}</dd>
    <dt>Endereço:</dt>
    <dd>{{ $cliente->endereco }}</dd>
    <dt>Criado em:</dt>
    <dd>{{ $cliente->created_at->format('d/m/Y h:i') }}</dd>
    <dt>Atualizado em:</dt>
    <dd>{{ $cliente->updated_at->format('d/m/Y h:i') }}</dd>
  </dl>
</div>
-->
@endsection