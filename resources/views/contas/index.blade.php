@extends('welcome')

@section('content')
  <div class="row">
<h1>conta</h1>
<hr />

<a href="/contas/create" class="btn btn-success pull-right">
  Cadastrar conta
</a>
  <br />
  <br />
  <br />
</div>

<div class="row">
  @if(Session::has('message'))
    <div class="alert alert-success">
      <em> {!! session('message') !!}</em>
      </div>
  @endif

<table class="table table-bordered">
  <tr>
  <th>ID</th>
  <th>Saldo</th>
  <th>ID Cliente</th>
  <th>Atualizar Cadastro</th>
  </tr>

    @foreach($contas as $conta)
    <tr>
      <td>{{ $conta -> id}}</td>
      <td>{{ $conta -> saldo}}</td>
      <td>{{ $conta -> cliente_id}}</td>
      
      <td>
        <a href="/contas/{{$conta->id}}" class="btn btn-default"
          arial-label="Mostrar Cliente">
          <span class="glyphicon glyphicon-eye-open"
          arial-hidden="true"></span>
        </a>
      </td>
    </tr>

    @endforeach
  </table>
</div>
@endsection