<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Produtocomprado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('produtoscomprados', function(Blueprint $table){
            $table->increments('id');
            $table->integer('idproduto')->unsigned();
            $table->foreign('idproduto')->references('id')->on('products');
            $table->decimal('valor', 5, 2);
            $table->integer('idvendas')->unsigned();
            $table->integer('quantia');
            $table->foreign('idvendas')->references('id')->on('vendas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('produtoscomprados');
    }
}
